<?php

namespace App\Form;

use App\Entity\BlogPost;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostProcessType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('summary', TextType::class)
            ->add('content', TextareaType::class)
            ->add('is_visible', CheckboxType::class, [
                "required" => false
            ])
            ->add('is_loggeg_only', CheckboxType::class, [
                "required" => false
            ])
            ->add('can_comment', CheckboxType::class, [
                "required" => false
            ])
            ->add('can_comment_unlogged', CheckboxType::class, [
                "required" => false
            ])
            ->add($options["submit"], SubmitType::class);

            $builder->addEventListener(FormEvents::POST_SUBMIT, function(FormEvent $e) {
                $entity = $e->getData();
                $form = $e->getForm();
                $isVisible = empty($_POST[$form->getName()]['is_visible']) ? false : true;
                $entity->setIsVisible($isVisible);

                $isLogged = empty($_POST[$form->getName()]['is_loggeg_only']) ? false : true;
                $entity->setIsLoggegOnly($isLogged);

                $canComment = empty($_POST[$form->getName()]['can_comment']) ? false : true;
                $entity->setCanComment($canComment);

                $canCommentUnlogged = empty($_POST[$form->getName()]['can_comment_unlogged']) ? false : true;
                $entity->setCanComment($canCommentUnlogged);
            });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogPost::class,
            "submit" => "Submit"
        ]);
    }
}
