<?php

namespace App\Controller;

use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Form\PostProcessType;
use App\Repository\BlogPostRepository;
use App\Repository\CommentRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostController extends AbstractController
{
    /**
     * @Route("/post", name="post")
     */
    public function index()
    {
        return $this->render('post/index.html.twig', [
            'controller_name' => 'PostController',
        ]);
    }

    /**
     * @Route("posts/list", name="post_listing")
     *
     * @return Response
     * @throws Exception
     */
    public function listBlogPosts(BlogPostRepository $repository)
    {
        $user = $this->getUser();

        $articles = $repository->getPosts($user != null, true);

        return $this->render('eti/blog/posts.html.twig', [
            'articles' => $articles
        ]);
    }


    /**
     * @Route("posts/view/{id}", name="post_details")
     *
     * @return Response
     * @throws Exception
     */
    public function postDetails(int $id, Request $request, BlogPostRepository $repository, UserRepository $userRepository, CommentRepository $commentRepository)
    {
        $article = $repository->find($id);

        if ($article == null) {
            return $this->redirectToRoute("homepage");
        }

        $user = $this->getUser();
        $author = $userRepository->find($article->getCreatorId());
        $comments = $commentRepository->findByArticleId($id);

        $commentForm = "Comments are disabled in this article";

        if ($article->getCanComment()) {
            $comment = new Comment();
            if ($user != null) {
                $comment->setFirstName($user->getFirstName());
                $comment->setLastName($user->getLastName());
            }

            $form = $this->createForm(CommentType::class, $comment, ["isUserLogged" => $user != null]);

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $comment->setArticleId($id);
                
                $comment->setCreatorId($user != null ? $user->getId() : null);
                $comment->setCreationDate(new \DateTime());

                $manager = $this->getDoctrine()->getManager();
                $manager->persist($comment);
                $manager->flush();
                $comments = $commentRepository->findByArticleId($id);
            }

            $commentForm = $form->createView();

            if (!$article->getCanCommentUnlogged() && $user == null)
                $commentForm = "You cannot comment this post. Please login";
        }

        return $this->render('eti/blog/post_view.html.twig', [
            'article' => $article,
            'author' => $author,
            'user' => $user,
            'comments' => $comments,
            'commentForm' => $commentForm,
            'isCommentFormString' => gettype($commentForm) == "string"
        ]);
    }

    /**
     * @Route("posts/edit/{id}", name="post_edit")
     *
     * @return Response
     * @throws Exception
     */
    public function postEdit(int $id, Request $request, BlogPostRepository $repository, UserRepository $userRepository)
    {
        $post = $repository->find($id);

        if ($post == null) {
            return $this->redirectToRoute("homepage");
        }

        $user = $this->getUser();

        $author = $userRepository->find($post->getCreatorId());

        if ($user == null || $user->getId() != $author->getId()) {
            return $this->redirectToRoute("homepage");
        }

        $form = $this->createForm(PostProcessType::class, $post, ["submit" => "Edit"]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setTitle($form->get('title')->getData());
            $post->setSummary($form->get('summary')->getData());
            $post->setContent($form->get('content')->getData());
            $post->setIsVisible($form->get('is_visible')->getData());
            $post->setIsLoggegOnly($form->get('is_loggeg_only')->getData());
            $post->setCanComment($form->get('can_comment')->getData());
            $post->setCanCommentUnlogged($form->get('can_comment_unlogged')->getData());

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($post);
            $manager->flush();
            return $this->redirectToRoute("post_listing");
        }

        return $this->render('post/process.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("posts/add", name="post_add")
     *
     * @return Response
     * @throws Exception
     */
    public function postAdd(Request $request, BlogPostRepository $repository, UserRepository $userRepository)
    {
        $user = $this->getUser();

        if ($user == null) {
            return $this->redirectToRoute("homepage");
        }

        $form = $this->createForm(PostProcessType::class, null, ["submit" => "Add"]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post = new BlogPost();
            $post->setCreatorId($user->getId());
            $post->setTitle($form->get('title')->getData());
            $post->setSummary($form->get('summary')->getData());
            $post->setContent($form->get('content')->getData());
            $post->setIsVisible($form->get('is_visible')->getData());
            $post->setIsLoggegOnly($form->get('is_loggeg_only')->getData());
            $post->setCanComment($form->get('can_comment')->getData());
            $post->setCanCommentUnlogged($form->get('can_comment_unlogged')->getData());
            $post->setCreationDate(new \DateTime());

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($post);
            $manager->flush();
            return $this->redirectToRoute("post_listing");
        }

        return $this->render('post/process.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }
}
